  /* Floating labels in request form*/

  (function() {
    const fields = document.querySelectorAll(".js-float-field");

    if (fields.length > 0) {
        floatLabels();
    }

    function floatLabels() {
        fields.forEach(function(field) {
            let input = field.querySelector('.js-float-input');
            let label = field.querySelector('.js-float-label');

            input.addEventListener("focus", function () {
                checkFocus(label);
            });
        
            input.addEventListener("blur", function () {
                checkBlur(input, label);
            });
        
            input.addEventListener("change keyup", function () {
                checkKeyup(input, label);
            });
        });
    }

    function checkFocus(label) {
        label.classList.add("float");
        label.classList.add("float--focus");
    }

    // function outsideClickHandler(evt) {
    //     let block = document.querySelector('.dropdown.active');
    //     console.group();
    //     console.log(block);
    //     console.log(block.contains(evt.target));
    //     console.log(block.querySelector('input').value === "");
    //     console.groupEnd();
    //     // console.

    //     if (block && !block.contains(evt.target) && block.querySelector('input').value === "") {
    //         block.querySelector('label').classList.remove("float");
    //     }
    // }

    // document.addEventListener('click', (evt) => {
    //     outsideClickHandler(evt);
    // });

    // (() => {
    //     let block = null;
        
    //     document.addEventListener('click', (evt) => {
    //         block = document.querySelector('.dropdown.active');
    
    //         if (block && !block.contains(evt.target)) {
    //             block.classList.remove('active');
    //         }
    //     });
    // })();
 
  
    function checkBlur(input, label) {
        label.classList.remove("float--focus");
        // console.log(input.parentNode);
        // console.log(input.parentNode.classList.contains('text-active'));

        window.setTimeout(() => {
            if (input.value === "") {
                label.classList.remove("float");
            }
        }, 100);
    }
    
    function checkKeyup(input, label) {
        if (input.value === "" && !input.focus()) {
            label.classList.remove("float");
        } else {
            label.classList.add("float");
        }
      }
  })();