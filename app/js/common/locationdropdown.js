(() => {
    const form = document.querySelector('.request__form');
    const inputs = form.querySelectorAll('.js-acomplete-input');
    const url = 'russia.json';
    const method = 'GET';
    const arrays = {
        cities: [],
    }

    let closeHandler = false;

    axios.get(url)
    .then(function (response) {
        // handle success
        getCities(response.data);

        inputs.forEach((input) => {
            if (input.dataset.acompleteList) {
                autocomplete(input, input.dataset.acompleteList.split(','));
            } else if (input.dataset.acompleteArray) {
                autocomplete(input, arrays[input.dataset.acompleteArray]);
            } else {
                console.warn(`список для меню input'a ${input.id} не найден`);
            }
        });
    })
    .catch(function (error) {
        // handle error
    })
    .then(function () {
        // always executed
    });

    function getCities(array) {
        array.forEach((el) => {
            arrays.cities.push(el.city);
        })
    }

    function autocomplete(input, array) {
        let currentFocus;

        input.addEventListener('click', function(event) {
            const inputValue = this.value;

            if (array.length > 10) {
                if (!inputValue) {
                    return false;
                }
            }

            if (!input.parentNode.querySelector('.autocomplete-list')) {

                createDropdownList(array, inputValue);

            }
        });

        input.addEventListener('input', function(event) {
            const inputValue = this.value;

            if (!inputValue) {
                return false;
            }

            closeAllLists();

            currentFocus = -1;

            createDropdownList(array, inputValue, true)
                
        });

        input.addEventListener('keydown', function(event) {
            let x = document.getElementById(this.id + 'autocomplete-list');

            if (x) {
                x = x.getElementsByTagName('div');
            }

            if (event.keyCode == 13) { //enter
                event.preventDefault();
                if (currentFocus > -1) {
                    if (x) x[currentFocus].click();
                }
            }
        });

        function createDropdownList(array, inputValue, typing) {

            const dropdownList = document.createElement('DIV');
            dropdownList.setAttribute('id', input.id + '-autocomplete-list');
            dropdownList.classList.add('autocomplete-list'); 
            input.parentNode.appendChild(dropdownList);

            array.forEach((item, index) => {
                    if (item.substr(0, inputValue.length).toUpperCase() == inputValue.toUpperCase()) {
                        const dropdownItem = document.createElement('DIV');
                        dropdownItem.classList.add('autocomplete-item');
                        dropdownItem.insertAdjacentHTML('afterbegin', '<strong>' + item.substr(0, inputValue.length) + '</strong>');
                        dropdownItem.insertAdjacentHTML('beforeend', item.substr(inputValue.length));
                        dropdownItem.insertAdjacentHTML('beforeend', '<input type="hidden" value="' + item + '">');
                        dropdownList.appendChild(dropdownItem);

                        dropdownItem.addEventListener('click', function(e) {
                            input.value = this.getElementsByTagName('input')[0].value;
                            closeAllLists();
                        });
                    }
            });
        }

        function closeAllLists(element) {
            var x = document.getElementsByClassName('autocomplete-list');

            Array.from(x).forEach((item) => {
                if (element != item && element != item.parentNode.querySelector('input')) {
                    item.parentNode.removeChild(item);
                }
            });
        }

        if (!closeHandler) {
            document.addEventListener('click', closeListsHandler);
            closeHandler = true;
        }

        function closeListsHandler(event) {
            closeAllLists(event.target);
        }
    }

})();