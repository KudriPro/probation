
// (() => {
    const modalStates = {
        hidden: 'popup-modal-hidden',
        active: 'modal-box-active',
        ready: 'modal-box-ready',
        noscroll: 'modal-box-viewed'
    }


    const animateEnd = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
    const modalOpenTriggers = document.querySelectorAll('.js-show-modal');
    // const modalBox = document.querySelector('.modal-box');

    modalOpenTriggers.forEach((el) => {
        el.addEventListener('click', function(evt) {
            evt.preventDefault();
            evt.stopPropagation();
            
            const modalId = this.dataset.modalId;
    
            openModal(modalId);
        });
    })

    function openModal(id) {

        let modal = document.querySelector(`${id}`);

        if (modal) {
            let modalWrapper = modal.parentNode;

            modal.classList.add(modalStates.active);

            document.body.classList.add(modalStates.noscroll);

            modalWrapper.classList.remove(modalStates.hidden);

            modalWrapper.addEventListener(animateEnd, function(evt) {
                modal.classList.add(modalStates.ready);
            });

            let modalCloseTriggers = modal.querySelectorAll('.js-close-modal');

            modalCloseTriggers.forEach((el) => {
                el.addEventListener('click', closeTriggerHandler);
            });

            document.addEventListener('click', outsideClickHandler);

            window.addEventListener('keydown', escKeydownHandler);

            function closeTriggerHandler(evt) {
                evt.preventDefault();
                
                closeModal(modal);
            }
        
            function outsideClickHandler(evt) {
                if (!modal.contains(evt.target)) {
                    closeModal(modal);
                }
            }

            function escKeydownHandler(evt) {
                if (evt.key === 'Escape') {
                    closeModal(modal);
                }
            }

            function closeModal(el) {
                document.body.classList.remove(modalStates.noscroll);
                el.parentNode.classList.add(modalStates.hidden);
                el.classList.remove(modalStates.active);
                el.classList.remove(modalStates.ready);
        
                modalCloseTriggers.forEach((el) => {
                    el.removeEventListener('click', closeTriggerHandler);
                });
                document.removeEventListener('click', outsideClickHandler);
                window.removeEventListener('keydown', escKeydownHandler);
            }
        } else {
            console.log('modal id is not found');
        }
    }
// })();