  /* Textarea autoresize by typing */
  document.addEventListener('DOMContentLoaded', function(){

    const textareas = document.querySelectorAll('.js-textarea-autoresize');
    const INITIAL_HEIGHT = 5;
      
    const observeEvent = function (element, event, handler) {
        element.addEventListener(event, () => {
            handler(element);
        });
    };
  
    function resize(el) {
        el.style.height = el.scrollHeight + 'px';

        // if (el.value === '') {
        //     el.style.height = INITIAL_HEIGHT + 'rem';
        // }
    }

    /* 0-timeout to get the already changed text */
    function delayedResize(input) {
        window.setTimeout(() => {
            resize(input);
        }, 0);
    }

    function initTextareaAutoresize() {

      textareas.forEach((input) => {
        observeEvent(input, 'change', resize);
        observeEvent(input, 'cut', delayedResize);
        observeEvent(input, 'paste', delayedResize);
        observeEvent(input, 'drop', delayedResize);
        observeEvent(input, 'keydown', delayedResize);

        resize(input);
      })
    }
  
    if (textareas.length > 0) {
        initTextareaAutoresize()
    };
  }); 