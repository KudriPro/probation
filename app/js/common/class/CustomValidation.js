class CustomValidation {
  
    constructor() {
      this.invalidities = [];
    }
  
    checkValidity(input, inputIsBlurred = false) {
  
      let validity = input.validity;
  
      if (validity.patternMismatch) {
        this.addInvalidity(input.dataset.errorMessage);
      }
  
      if (validity.tooLong) {
        var maxlength = input.getAttribute('maxlength');
        this.addInvalidity('Количество символов не должно быть больше ' + maxlength);
      }
  
      if (validity.tooShort) {
        var minlength = input.getAttribute('minlength');
        this.addInvalidity('Количество символов не должно быть меньше ' + minlength);
      }
  
      // if (validity.typeMismatch) {
      //   var type = input.getAttribute('type');
      //   this.addInvalidity('This number needs to be a multiple of ' + type);
      // }
  
      if (validity.valueMissing && !inputIsBlurred) {
          this.addInvalidity('Поле необходимо заполнить');
      }

    };
  
    addInvalidity(message) {
      this.invalidities.push(message);
    };

    getInvalidities() {
      return this.invalidities.join('. \n');
    };
  
      getInvaliditiesForHTML() {
          return this.invalidities.join('. <br>');
      }
  };