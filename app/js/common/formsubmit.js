function submitForm(submit) {
    // const form = document.querySelector('.request__form');
    // const submit = form.querySelector('.request__btn');
    const url = 'russia.json';

    // submit.addEventListener('click', function(event) {
    //     event.preventDefault();

        const modalId = submit.dataset.modalId;
        submit.disabled = true;

        axios.get(url)
        .then(function (response) {
            // handle success
            openModal(modalId);
        })
        .catch(function (error) {
            // handle error
        })
        .then(function () {
            // always executed
            submit.disabled = false;
        });
    // });
}