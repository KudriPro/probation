/* forEach polyfill*/

if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

/* Object.assign polyfill*/

if (typeof Object.assign != 'function') {
    Object.assign = function(target) {
      'use strict';
      if (target == null) {
        throw new TypeError('Cannot convert undefined or null to object');
      }
  
      target = Object(target);
      for (var index = 1; index < arguments.length; index++) {
        var source = arguments[index];
        if (source != null) {
          for (var key in source) {
            if (Object.prototype.hasOwnProperty.call(source, key)) {
              target[key] = source[key];
            }
          }
        }
      }
      return target;
    };
  }

/* Swiper */

const swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    spaceBetween: 29,
    // slidesPerView: 1,

    breakpoints: {
        768: {
          spaceBetween: 5,
        //   slidesPerView: 1
        }
    }
  });

/* Device detecting */

function detectDevice() {
    let deviceOs = getMobileOs();
    document.body.classList.add('platform_' + deviceOs);
}

function getMobileOs(){
    let userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if(/android/i.test(userAgent)) {return 'android'}
    if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream){return 'ios'}
    return 'unknown'
}

/* Sticky nav */
(function() {
    const stickyContainer = document.querySelector('.js-sticky-nav');

    const sections = document.querySelectorAll('.js-observed-block')
    const pageHeader = document.querySelector('.page-header');

    let stickyHeight = null;
    let mainContainerRootMargin = null;
    let sectionObserverRootMargin = null;
    let isDesktop = false;
    let OFFSET = -10;

    let mainContainerObserver = null;
    let sectionObserver = null;

    function calcMargins() {
        stickyHeight = stickyContainer.offsetHeight;
        mainContainerRootMargin = -stickyHeight + OFFSET + "px 0px 0px 0px";
        sectionObserverRootMargin = -stickyHeight + OFFSET + "px 0px -50% 0px";
    }; 

    function setObservers() {
        calcMargins();

        mainContainerObserver = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    stickyContainer.classList.remove('sticky-nav--visible');
                } else {
                    stickyContainer.classList.add('sticky-nav--visible');
                }
            })
        }, {rootMargin: mainContainerRootMargin});
    
        sectionObserver = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    const currentSection = entry.target;
                    const activeItem = stickyContainer.querySelector('.js-sticky-nav-item--current');
                    const currentItem = stickyContainer.querySelector('[data-target=' + currentSection.id + ']');
    
                    if (activeItem) {
                        activeItem.classList.remove('js-sticky-nav-item--current');
                    }
    
                    if (currentItem) {
                        currentItem.classList.add('js-sticky-nav-item--current');
                    }
    
                } else {
                    const currentItem = stickyContainer.querySelector('[data-target=' + entry.target.id + ']');
                    currentItem.classList.remove('js-sticky-nav-item--current');
                }
            })
        }, { rootMargin: sectionObserverRootMargin});
    }

    function initStickyNav() {
            setObservers();

            isDesktop = true;
            mainContainerObserver.observe(pageHeader);
            sections.forEach((section) => {
                sectionObserver.observe(section);
            })
    };


    function updateStickyNav() {
        if (window.matchMedia("(min-width: 768px)").matches) {
            isDesktop = true;
            setObservers();

            mainContainerObserver.observe(pageHeader);
            sections.forEach((section) => {
                sectionObserver.observe(section);
            })
        } else if (isDesktop && window.matchMedia("(max-width: 767px)").matches) {
            stickyContainer.classList.remove('sticky-nav--visible');
            mainContainerObserver.disconnect();
            sectionObserver.disconnect();
        }
    }

    if (window.matchMedia("(min-width: 767px)").matches) {
        document.addEventListener('DOMContentLoaded', initStickyNav);
    }
    
    window.addEventListener('resize', updateStickyNav);
})();

/* Anchor links*/

(function() {
    const links = document.querySelectorAll('.js-anchor-link');
    const stickyNav = document.querySelector('.sticky-nav');
    const mainNav = document.querySelector('.main-nav');
    let offset = null;

    function getYOffset(el) {
        const rect = el.getBoundingClientRect();
        return rect.top + window.scrollY;
    }

    function doScrolling(element, duration, offset) { 
        const elementY = getYOffset(element);
        const startingY = window.pageYOffset;
        const diff = elementY - startingY;
        let start;
      
        window.requestAnimationFrame(function step(timestamp) {
          if (!start) start = timestamp;
          const time = timestamp - start;
          const percent = Math.min(time / duration, 1);

          window.scrollTo(0, (startingY + offset + diff * percent));
      
          if (time < duration) {
            window.requestAnimationFrame(step);
          }
        })
    }

    function calcOffset() {
        if (window.matchMedia('(min-width: 768px)').matches && stickyNav) {
            offset =  -stickyNav.offsetHeight;
        } else {
            offset = -mainNav.offsetHeight;
        }
    }

    function initAnchorLinks() {
        links.forEach((link) => {
            link.addEventListener('click', (evt) => {
                evt.preventDefault();

                // calcOffset()
                
                let target = document.querySelector('#' + link.dataset.target);
                target.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start'
                  });
                // doScrolling(target, 400, offset);
            });
        });
    }

    function updateAnchorLinks() {
        calcOffset();
    }

    // if (links.length > 0) {
    //     initLinks();
    // }

    document.addEventListener('DOMContentLoaded', initAnchorLinks);
    window.addEventListener('resize', updateAnchorLinks);
})();

/* Mobile menu */

(function() {
        const pageHeader = document.querySelector('.page-header');
        const teaserContainer = pageHeader.querySelector('.teaser');
        const barContainer = pageHeader.querySelector('.main-nav');
        const menuContainer = barContainer.querySelector('.main-nav__menu');
        const menuItems = barContainer.querySelectorAll('.js-nav-link');
        const menuToggleBtn = barContainer.querySelector('.js-mobile-menu-btn');
        let handlersAreReady = false;

        function setMenu() {
            const menuOffsetTop = barContainer.offsetHeight;
            menuContainer.style.paddingTop = menuOffsetTop + 'px';
            teaserContainer.style.paddingTop = menuOffsetTop + 'px';
        };

        function toggleMenuHandler(evt) {
            evt.preventDefault();
            menuContainer.classList.toggle('main-nav__menu--visible');
            menuToggleBtn.classList.toggle('opened');
            document.body.classList.toggle('main-nav-active');
        };

        function closeMenuHandler(evt) {
            evt.preventDefault();
            menuContainer.classList.remove('main-nav__menu--visible');
            menuToggleBtn.classList.remove('opened');
            document.body.classList.remove('main-nav-active');
        };

        function toggleMenu(evt, trigger) {
            trigger.addEventListener(evt, toggleMenuHandler);
        };

        function closeMenu(evt, trigger) {
            trigger.addEventListener(evt, closeMenuHandler);
        };
        
        function addHandlers() {
            toggleMenu('click', menuToggleBtn);

            menuItems.forEach((el) => {
                closeMenu('click', el);  
            });

            handlersAreReady = true;
        }

        function initMobileMenu() {
            setMenu();
            addHandlers();
        }

        function updateMobileMenu() {
            if (window.matchMedia("(max-width: 767px)").matches) {
                setMenu();

                if (!handlersAreReady) {
                    addHandlers();
                }
            } else {
                menuContainer.classList.remove('main-nav__menu--visible');
                menuToggleBtn.classList.remove('opened');
                document.body.classList.remove('main-nav-active');
                teaserContainer.style.paddingTop = 0;
            }
        };

        if (window.matchMedia("(max-width: 767px)").matches) {
            document.addEventListener('DOMContentLoaded', initMobileMenu);
        }

        window.addEventListener('resize', updateMobileMenu);
})();

/* Directions menu */

function switchDirections() {
    const navs = document.querySelectorAll('.directions__nav-item');
    const blocks = document.querySelectorAll('.directions__item');

    navs.forEach((nav) => {
        nav.addEventListener('click', () => {
            navs.forEach((nav) => {
                nav.classList.remove('directions__nav-item--active');
            });

            blocks.forEach((block) => {
                block.classList.remove('directions__item--visible');

                if (block.id === nav.dataset.directions) {
                    block.classList.add('directions__item--visible');
                    nav.classList.add('directions__nav-item--active');
                }
            });
        })
    });
};

/* Accordion */

function animateAccordion() {
    const accordion = document.querySelectorAll(".js-accordion");

    accordion.forEach((el) => {
        let control = el.querySelector('.js-accordion-control'); 

        control.addEventListener('click', function() {
            el.classList.toggle('active');

            let panel = this.nextElementSibling;

            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
              } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
              }
        });
    });
}

/*Lazy load*/

function initLazyLoad() {
    const images = document.querySelectorAll('img.lazyload')

    if ('loading' in HTMLImageElement.prototype) {
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    } else {
        const imageObserver = new IntersectionObserver((entries, imgObserver) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    const lazyImage = entry.target
                    lazyImage.src = lazyImage.dataset.src
                    lazyImage.classList.remove("lazyload");
                    imgObserver.unobserve(lazyImage);
                }
            })
        });

        images.forEach((img) => {
            imageObserver.observe(img);
        })
    }
};


/* Close form dropdown */

(() => {
    let block = null;
    
    document.addEventListener('click', (evt) => {
        block = document.querySelector('.dropdown.active');

        if (block && !block.contains(evt.target)) {
            block.classList.remove('active');
        }
    });
  })();

/* Common init */

// $(function(){
//     detectDevice();
//     // initMobileMenu();
//     // initAnchorLinks();
//     // initStickyNav();
//     switchDirections();
//     animateAccordion();
//     svg4everybody({
//         polyfill: true // polyfill <use> elements for External Content
//     });
    
//     new WOW().init();
// });

(() => {
    function initCommonScripts() {
        detectDevice();
        switchDirections();
        animateAccordion();
        
        svg4everybody({
            polyfill: true // polyfill <use> elements for External Content
        });
        
        new WOW().init();
    };

    document.addEventListener('DOMContentLoaded', initCommonScripts);
})();