const form = document.querySelector('.request__form');
const inputs = document.querySelectorAll('input');
const submit = form.querySelector('.request__btn');
let isInvalid = false;

function checkElementValidity(el, inputIsBlurred) {
    let errorList = el.parentNode.querySelector('.error-list');

    if (errorList) {
        errorList.remove();
        el.classList.remove('is-invalid');
    }

    if (el.checkValidity() == false) {

        var inputCustomValidation = new CustomValidation();
        inputCustomValidation.checkValidity(el, inputIsBlurred);
        var customValidityMessage = inputCustomValidation.getInvalidities();
        el.setCustomValidity(customValidityMessage);

        var customValidityMessageForHTML = inputCustomValidation.getInvaliditiesForHTML();

        if (customValidityMessageForHTML.length > 0) {
            el.insertAdjacentHTML('afterend', '<div class="error-list"></div>')
            el.parentNode.querySelector('.error-list').insertAdjacentHTML('afterbegin', '<span class="error-text"><span>' + customValidityMessageForHTML + '</span></span>')
            el.classList.add('is-invalid');
        }

        isInvalid = true;

    }
}

inputs.forEach((input) => {
    input.addEventListener('blur', function(event) {
        window.setTimeout(() => {
            checkElementValidity(this, true);
        }, 100);
    })
});

submit.addEventListener('click', function(e) {
    isInvalid = false;

    inputs.forEach((input) => {
        checkElementValidity(input);
    });

    if (!isInvalid) {
        submitForm(this);
    }
});